
<?php

$this->group([
    'namespace' => 'ClientBase'
],function($route){
    $route->screen('clientbase/clients/{client}/edit', 'Clients\ClientsEdit','dashboard.clientbase.clients.edit');
    $route->screen('clientbase/clients', 'Clients\ClientsList','dashboard.clientbase.clients.list');

    $route->screen('clientbase/service/create', 'Service\ServiceEdit','dashboard.clientbase.service.create');
    $route->screen('clientbase/service/{service}/edit', 'Service\ServiceEdit','dashboard.clientbase.service.edit');
    $route->screen('clientbase/service', 'Service\ServiceList','dashboard.clientbase.service.list');

    $route->screen('clientbase/requestWork/create', 'RequestWork\AddRequestEdit','dashboard.clientbase.requestwork.create');
    $route->screen('clientbase/requestWork', 'RequestWork\AddRequest','dashboard.clientbase.requestwork.list');

});