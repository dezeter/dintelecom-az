<?php

namespace App\Layouts\ClientBase\Clients;

use App\Http\Filters\ClientName;
use Orchid\Platform\Platform\Fields\TD;
use Orchid\Platform\Http\Filters\CreatedFilter;
use Orchid\Platform\Http\Filters\SearchFilter;
use Orchid\Platform\Http\Filters\StatusFilter;
use Orchid\Platform\Layouts\Table;

class ClientList extends Table
{

    /**
     * @var string
     */
    public $data = 'clients';

    /**
     * HTTP data filters
     *
     * @return array
     */
    public function filters() : array
    {
        return [
            ClientName::class,
            SearchFilter::class,
            CreatedFilter::class,
        ];
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
        TD::name('name')
            ->title('Имя клиента')
            ->setRender(function ($client) {
                    return '<a href="' . route('dashboard.clientbase.clients.edit',
                            $client->id) . '">' . $client->name . '</a>';
                }),

        TD::name('email')
            ->title('Email'),

        TD::name('phone')
            ->title('Phone'),
        TD::name('Adress'),
        ];
    }
}
