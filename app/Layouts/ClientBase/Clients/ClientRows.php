<?php

namespace App\Layouts\ClientBase\Clients;

use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class ClientRows extends Rows
{
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields() : array
    {
        return [
        	Field::tag('input')
        			->type('text')
        			->name('client.name')
        			->max(255)
        			->required()
        			->title('Имя клиента'),

        	Field::tag('input')
        			->type('input')
        			->name('client.email')
        			->max(255)
        			->required()
        			->title('Email'),

        	Field::tag('input')
        			->type('text')
        			->name('client.phone')
        			->max(12)
        			->required()
        			->title('Номер телефона')
        			->mask('(999) 999-9999'),

        	Field::tag('input')
        			->type('text')
        			->name('client.adress')
        			->max(255)
        			->required()
        			->title('Адресс клиента'),
        ];
    }
}
