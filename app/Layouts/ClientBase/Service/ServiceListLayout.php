<?php

namespace App\Layouts\ClientBase\Service;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ServiceListLayout extends Table
{

    /**
     * @var string
     */
    public $data = 'services';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('name')
                ->title('Название услуги')
                ->setRender(function ($service) {
                    return '<a href="' . route('dashboard.clientbase.service.edit',
                            $service->id) . '">' . $service->name . '</a>';
                }),
            TD::name('descriptions')
                ->title('Описание Услуги'),
        ];
    }
}
