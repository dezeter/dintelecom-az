<?php

namespace App\Layouts\ClientBase\Service;

use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class ServiceRows extends Rows
{
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [

        	Field::tag('input')
        			->type('text')
        			->name('service.name')
        			->max(255)
        			->required()
        			->title('Название Услуги'),

        	Field::tag('textarea')
        			->name('service.descriptions')
        			->required()
        			->title('Описание Услуги')
        			->row(10),
        ];
    }
}
