<?php

namespace App\Layouts\ClientBase\RequestWork;

use App\Core\Models\Service;
use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class RequestList extends Table
{

    /**
     * @var string
     */
    public $data = 'request';

    protected $_request_types = [
        [
            'id'   => 0,
            'text' => 'Не выполнен',
        ],
        [
            'id'   => 1,
            'text' => 'Выполнен',
        ],
    ];
    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('request_type')
                ->title('Тип запроса')
                ->setRender(function ($row) {
                    return Service::where('id', $row->request_type)->first()->name;
                }),
    
            TD::name('request_discription')
                ->title('Описание'),

            TD::name('request_status')
                ->title('Статус')
                ->setRender(function ($request) {
                    $types = $this->_request_types;
                    $message = isset($types[$request->request_status]['text']) ? $types[$request->request_status]['text'] : '';
                    return $message;
                }),

            TD::name('request_client_id')
                ->title('Клиент')
                ->setRender(function ($row) {
                    return $row->client->name;
                })
        ];
    }
}