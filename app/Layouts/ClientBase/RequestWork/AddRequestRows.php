<?php

namespace App\Layouts\ClientBase\RequestWork;

use App\Listeners\ServiceListener;
use App\Layouts\ClientBase\Service\ServiceListLayout;
use App\Core\Models\Service;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;
use Illuminate\Support\Facades\Auth;

class AddRequestRows extends Rows
{
    
    /**
     * Views
     *
     * @return array
     */
    public function fields(): array
    {
        return [
        	Field::tag('select')
                ->options(Service::all()->pluck("name", "id"))
        		->name('requestwork.request_type')
        		->title('Выбрать услугу'),

            Field::tag('textarea')
                ->title('Дополнительная информация')
                ->name('requestwork.request_discription'),

            Field::tag('input')
                ->name('requestwork.request_status')
                ->title('adas'),
        ];
    }
}
