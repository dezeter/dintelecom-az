<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service';
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'descriptions',
    ];
    /**
     * @param $key
     *
     * @return mixed
     */
    public function getContent($key){
        return $this->getAttribute($key);
    }

    /**
     * Share relationship
     */
    public function service(){
        return $this->belongTo('App\Core\Models\Service', 'request_type');
    }
}