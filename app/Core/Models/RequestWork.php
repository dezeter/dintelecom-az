<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;

class RequestWork extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'requestwork';

     /**
     * @var array
     */
    protected $fillable = [
    	'request_type',
    	'request_discription',
    	'request_status',
    	'request_client_id',
    ];

    /**
     * @param $key
     *
     * @return mixed
     */
    public function getContent($key){
        return $this->getAttribute($key);
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'request_client_id');
    }
}
