<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Orchid\Platform\Core\Traits\FilterTrait;

class Client extends Model
{
    use FilterTrait;


    /**
    	* The database table used by the model.
    	*
    	* @var string
    	*/
    protected $table = 'client';

    /**
    	* @var array
    	*/
    protected $fillable = [
    	'name',
    	'email',
    	'phone',
    	'adress',
    ];

    /**
    * @param $key
    *
    * @return mixed
    */
  	public function getContent($key){
  		return $this->getAttribute($key);
  	}

}
