<?php

namespace App\Http\Controllers\Screens\ClientBase\Service;

use App\Layouts\ClientBase\Service\ServiceListLayout;
use App\Core\Models\Service;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class ServiceList extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Service Screen';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Service Screen';

    public $permission = "dicom-clients";

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'services' => Service::orderBy('id','Desc')->paginate()
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Create')->method('create'),
        ];
    }

    /**
     * Views
     *
     * @return array
     */
    public function layout() : array
    {
        return [
            ServiceListLayout::class,
        ];
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        return redirect()->route('dashboard.clientbase.service.create');
    }
}
