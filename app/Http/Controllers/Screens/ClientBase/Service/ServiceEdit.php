<?php

namespace App\Http\Controllers\Screens\ClientBase\Service;

use App\Core\Models\Service;
use App\Layouts\ClientBase\Service\ServiceRows;
use Illuminate\Http\Request;
use Orchid\Platform\Facades\Alert;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class ServiceEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Service card';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Service';

    /**
     * Query data
     *
     * @return array
     */
    public function query($service = null) : array
    {
        return [
            'service' => is_null($service) ? new Service() : $service,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Save')->method('save'),
            Link::name('Remove')->method('remove'),
        ];
    }

    /**
     * Views
     *
     * @return array
     */
    public function layout() : array
    {
        return [
            ServiceRows::class,
        ];
    }

    /**
     * @param Service $service
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save($test,Service $service)
    {
        $service->fill($this->request->get('service'))->save();
        Alert::info('Услуга сохранена.');
        return redirect()->route('dashboard.clientbase.service.list');
    }

    /**
     * @param Service $service
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Service $service)
    {
        $service->delete();
        Alert::info('Message');
        return redirect()->route('dashboard.clientbase.service.list');
    }
}
