<?php
namespace App\Http\Controllers\Screens\ClientBase\Clients;

use App\Core\Models\Client;
use App\Layouts\ClientBase\Clients\ClientRows;
use Orchid\Platform\Facades\Alert;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class ClientsEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Client card';
    
    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'There is a record of the client\'s';

    /**
     * Query data
     *
     * @param Client $client
     *
     * @return array
     */
    public function query($client = null) : array
    {
        $client = is_null($client) ? new Client() : $client;

        return [
            'client'     => $client,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            //Link::name('Write out a bill')->method('save'),
            Link::name('Save')->method('save'),
            Link::name('Remove')->method('remove'),
        ];
    }
    /**
     * Views
     *
     * @return array
     */
    public function layout() : array
    {
        return [
            Layouts::columns([
                'Left column' => [
                    ClientRows::class,
                ],
            ]),
            // Modals windows
        ];
    }

    /**
     * @param CLient $client
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Client $client)
    {
        $client->fill($this->request->get('client'))->save();
        Alert::info('Message');

        return redirect()->route('dashboard.clientbase.clients.list');
    }

    /**
     * @param Client $client
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Client $client)
    {
        $client->delete();
        Alert::info('Message');

        return redirect()->route('dashboard.clientbase.clients.list');
    }
}