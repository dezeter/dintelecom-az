<?php

namespace App\Http\Controllers\Screens\ClientBase\Clients;

use App\Http\Filters\ClientName;
use App\Layouts\ClientBase\Clients\ClientRows;
use App\Layouts\ClientBase\Clients\ClientList;
use App\Core\Models\Client;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class ClientsList extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'ClientsList Screen';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'ClientsList Screen';

    public $permission = "dicom-clients";

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'clients' => Client::filtersApply([
                ClientName::class,
            ])->orderBy('id', 'Desc')->paginate()
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Добавить нового клиента')
                ->modal('create')
                ->title('Добавить нового клиента')
                ->method('create'),
        ];
    }

    /**
     * Views
     *
     * @return array
     */
    public function layout() : array
    {
        return [
            ClientList::class,

            //modal windows
            Layouts::modals([
                'create' => [
                    ClientRows::class,
                ],
            ]),
        ];
    }

    /**
    * @param         $method
    * @param Request $request
    *
    * @return \Illuminate\Http\RedirectResponse
    */
   public function create($method, Request $request)
   {
        $client = Client::create($request->get('client'));

        return redirect()->route('dashboard.clientbase.clients.edit', $client->id);
   }
}
