<?php

namespace App\Http\Controllers\Screens\ClientBase\RequestWork;

use App\Layouts\ClientBase\RequestWork\RequestList;
use App\Layouts\ClientBase\RequestWork\AddRequestRows;
use App\Core\Models\Service;
use Illuminate\Http\Request;
use App\Core\Models\RequestWork;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;
use Illuminate\Support\Facades\Auth;


class AddRequest extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'AddRequest Screen';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'AddRequest Screen';

    public $permission = "dicom-clients";

    public function authorize()
    {
        Auth:user()->hasAccess($string);
    }
    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'request' => RequestWork::orderBy('id','Desc')->paginate()
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Create')->method('create'),
        ];
    }

    /**
     * Views
     *
     * @return array
     */
    public function layout() : array
    {
        return [
            RequestList::class,
        ];
    }


    /**
    * @param         $method
    * @param Request $request
    *
    * @return \Illuminate\Http\RedirectResponse
    */

public function create()
    {
        return redirect()->route('dashboard.clientbase.requestwork.create');
    }
}