<?php
namespace App\Http\Controllers\Screens\ClientBase\RequestWork;

use App\Core\Models\RequestWork;
use App\Layouts\ClientBase\RequestWork\AddRequestRows;
use Orchid\Platform\Facades\Alert;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;
use Illuminate\Support\Facades\Auth;

class AddRequestEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Request card';
    
    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'There is a record of the reqest\'s';
    /**
     * Query data
     *
     * @param Client $requests
     *
     * @return array
     */
    public function query($requestwork = null) : array
    {
        return [
           $requestwork = is_null($requestwork) ? new RequestWork() : $requestwork,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            //Link::name('Write out a bill')->method('save'),
            Link::name('Save')->method('save'),
            Link::name('Remove')->method('remove'),
        ];
    }
    /**
     * Views
     *
     * @return array
     */
    public function layout() : array
    {
        return [
            Layouts::columns([
                'Left column' => [
                    AddRequestRows::class,
                ],
            ]),
            // Modals windows
        ];
    }

    /**
     * @param requests $requests
     *requests
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(RequestWork $requestwork)
    {
        $request_client_id = Auth::user()->id;
        $requestwork->fill($this->request->get('requestwork'))->save();
        Alert::info('Message');

        return redirect()->route('dashboard.clientbase.requestwork.list');
    }

    /**
     * @param Client $client
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(RequestWork $requestwork)
    {
        $requestwork->delete();
        Alert::info('Message');

        return redirect()->route('dashboard.clientbase.requestwork.list');
    }
}