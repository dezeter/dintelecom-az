<?php
namespace App\Http\Composer;

use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Kernel\Dashboard;
use Orchid\Platform\Notifications\DashboardNotification;

class MenuComposer
{
    /**
     * MenuComposer constructor.
     *
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->menu = $dashboard->menu;
    }
    public $permission = "dashboard.dicom-clients";
    /**
     *
     */
    public function compose()
    {
        $this->menu->add('Main', [
            'slug'   => 'dicom-clients',
            'icon'   => 'icon-chemistry',
            'active' => 'dashboard.clientbase.*',
            'route'  => '#',
            'label'  => 'Dicom Clients',
            'childs' => true,
            'main'   => true,
            'sort'   => 0,
        ]);
        $this->menu->add('dicom-clients', [
            'slug'      => 'RequestWork',
            'icon'      => 'icon-people',
            'active'    => 'dashboard.clientbase.*',
            'route'     => route('dashboard.clientbase.requestwork.list'),
            'label'     => 'Add Request',
            'groupname' => 'ClientsBase',
        ]);
        $this->menu->add('dicom-clients', [
            'slug'      => 'Element',
            'icon'      => 'icon-bag',
            'active' => 'dashboard.clientbase.*',
            'route'  => route('dashboard.clientbase.service.list'),
            'label'     => 'Service List',
            'groupname' => 'Настройки сервиса',
            'divider'   => false,
            'childs'    => false,
            'sort'      => 1,
        ]);
    }
}